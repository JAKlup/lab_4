﻿using System;

namespace Task_2
{
    class Program_2
    {
        static double Method(double k)
        {
            double s = (11 * k) / 105;
            return s;
        }
        static double Rec(double k)
        {


            if (k == 0)
            {
                return Method(k);
            }
            else
            {
                return Method(k) + Rec(k - 1);
            }


        }
        static void Main(string[] args)
        {

            
            int n = Convert.ToInt32(Console.ReadLine());
            double s = Rec(n);


            Console.WriteLine(s);
            Console.ReadKey();
        }

    }
}

﻿using System;

namespace Task_3
{
    class Program_3
    {
        static double Method(double k)
        {
            double s = (3 * k - 13) / 31;
            return s;
        }
        static double Rec(double k)
        {


            if (k == -1)
            {
                return 1;
            }
            else
            {
                return Method(k) * Rec(k - 1);
            }


        }
        static void Main(string[] args)
        {


            int n = Convert.ToInt32(Console.ReadLine());
            double s = Rec(n);


            Console.WriteLine(s);
            Console.ReadKey();
        }

    }
}
